<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 04/02/19
 * Time: 21:26
 */

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JsonSerializable;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmpresaRepository")
 */
class Empresa implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=14, nullable=false)
     */
    private $cnpj;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $nomeFantasia;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $razaoSocial;

    /** @ORM\ManyToMany(targetEntity="Socio", mappedBy="empresas") */
    private $socios;

    public function __construct()
    {
        $this->socios = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCpnj(): ?string
    {
        return $this->cnpj;
    }

    /**
     * @param string $cnpj
     * @return $this
     */
    public function setCnpj(string $cnpj): self
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * @return string
     */
    public function getNomeFantasia(): ?string
    {
        return $this->nomeFantasia;
    }

    /**
     * @param string $nomeFantasia
     * @return $this
     */
    public function setNomeFantasia(string $nomeFantasia): self
    {
        $this->nomeFantasia = $nomeFantasia;

        return $this;
    }

    /**
     * @return string
     */
    public function getRazaoSocial(): ?string
    {
        return $this->razaoSocial;
    }

    /**
     * @param string $razaoSocial
     * @return $this
     */
    public function setRazaoSocial(string $razaoSocial): self
    {
        $this->razaoSocial = $razaoSocial;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getSocios(): ?Collection
    {
        return $this->socios;
    }

    /**
     * @param Collection $socios
     * @return Collection
     */
    public function setSocios(Collection $socios): Collection
    {
        $this->socios = $socios;

        return $this->socios;
    }

    /**
     * @param Socio $socio
     * @return Collection
     */
    public function addSocio($socio): Collection
    {
        $this->socios->add($socio);

        return $this->socios;
    }

    /**
     * @param Socio $socio
     * @return Collection
     */
    public function removeSocio($socio): Collection
    {
        $this->socios->removeElement($socio);

        return $this->socios;
    }

    /**
     * @param Socio $socio
     * @return bool
     */
    public function hasSocio($socio): bool
    {
        return $this->socios->contains($socio);
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'cnpj' => $this->cnpj,
            'nome_fantasia' => $this->nomeFantasia,
            'razao_social' => $this->razaoSocial,
            'socios'=> '',
        ];
    }
}


