<?php
/**
 * Created by PhpStorm.
 * User: matheus
 * Date: 04/02/19
 * Time: 21:57
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SocioRepository")
 */
class Socio implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=11, nullable=false)
     */
    private $cpf;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $nome;

    /** @ORM\ManyToMany(targetEntity="Empresa", inversedBy="socios") */
    private $empresas;

    public function __construct()
    {
        $this->empresas = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    /**
     * @param string $cpf
     * @return $this
     */
    public function setCpf(string $cpf): self
    {
        $this->cpf = $cpf;

        return $this;
    }

    /**
     * @return string
     */
    public function getNome(): ?string
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @return $this
     */
    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getEmpresas(): ?Collection
    {
        return $this->empresas;
    }

    /**
     * @param Collection $empresas
     * @return Collection
     */
    public function setEmpresas(Collection $empresas): ?Collection
    {
        $this->empresas = $empresas;

        return $this->empresas;
    }

    /**
     * @param Empresa $empresa
     * @return Collection
     */
    public function addEmpresa(Empresa $empresa): Collection
    {
        $this->empresas->add($empresa);
        $empresa->addSocio($this);

        return $this->empresas;
    }

    /**
     * @param Empresa $empresa
     * @return Collection
     */
    public function removeEmpresa(Empresa $empresa): Collection
    {
        $this->empresas->removeElement($empresa);
        $empresa->removeSocio($this);

        return $this->empresas;

    }

    /**
     * @param Empresa $empresa
     * @return bool
     */
    public function hasEmpresa($empresa): bool
    {
        return $this->empresas->contains($empresa);
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'cpf' => $this->cpf,
            'nome' => $this->nome,
            'empresas' => $this->empresas->toArray()
        ];
    }
}


