<?php

namespace App\Controller;

use App\Entity\Empresa;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Routing\Annotation\Route;

class EmpresaController extends AbstractController
{
    /**
     * @Route("/empresas", name="empresas")
     * @return mixed
     */
    public function index()
    {
        return $this->render('empresa/list_empresa.html.twig');
    }

    /**
     * @Route("/empresas/{id}", name="empresa_edit")
     * @param string $id
     * @return mixed
     */
    public function show($id)
    {
        $data = [
            'id' => $id
        ];

        return $this->render('empresa/edit_empresa.html.twig', [
            'data' => $data,
        ]);
    }
}
