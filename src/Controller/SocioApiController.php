<?php

namespace App\Controller;

use App\Entity\Empresa;
use App\Entity\Socio;
use App\Repository\EmpresaRepository;
use App\Repository\SocioRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SocioApiController extends AbstractController
{

    /**
     * @Route("/api/socios", name="socios_list", methods={"GET"})
     * @param SocioRepository $socioRepository
     * @return JsonResponse
     */
    public function list(SocioRepository $socioRepository): JsonResponse
    {
        return new JsonResponse($socioRepository->findAll());
    }

    /**
     * @Route("/api/socios/{id}", name="socios_show", methods={"GET"})
     * @param string $id
     * @param SocioRepository $socioRepository
     * @return JsonResponse
     */
    public function show($id, SocioRepository $socioRepository): JsonResponse
    {
        return new JsonResponse($socioRepository->findOneBy(['id' => $id]));
    }

    /**
     * @Route("/api/socios", name="socios_add", methods={"POST"})
     * @param Request $request
     * @param EmpresaRepository $empresaRepository
     * @return JsonResponse
     */
    public function add(Request $request, EmpresaRepository $empresaRepository): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        $socio = new Socio();
        $socio->setCpf($request->request->get('cpf'));
        $socio->setNome($request->request->get('nome'));
        $empresas = $request->request->get('empresas');

        if($empresas){
            foreach ($empresas as $empresaId)
                $socio->addEmpresa($empresaRepository->findOneBy(['id' => $empresaId]));
        }

        $em->persist($socio);
        $em->flush();

        return new JsonResponse($socio);
    }

    /**
     * @Route("/api/socios/{id}", name="socios_edit", methods={"PUT"})
     * @param string $id
     * @param Request $request
     * @param SocioRepository $socioRepository
     * @param EmpresaRepository $empresaRepository
     * @return JsonResponse
     */
    public function edit($id, Request $request, SocioRepository $socioRepository,
                         EmpresaRepository $empresaRepository): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        $socio = $socioRepository->findOneBy(['id' => $id]);
        $socio->setCpf($request->request->get('cpf'));
        $socio->setNome($request->request->get('nome'));
        $empresas = $request->request->get('empresas');
        if($empresas){
            $empresas_array = [];
            foreach ($empresas as $empresa_id){
                $empresa = $empresaRepository->findOneBy(['id' => $empresa_id]);
                array_push($empresas_array, $empresa);
            }

            $socio->setEmpresas(new ArrayCollection($empresas_array));
        }

        $em->flush();

        return new JsonResponse($socio);
    }

    /**
     * @Route("/api/socios/{id}", name="socios_remove", methods={"DELETE"})
     * @param string $id
     * @param SocioRepository $socioRepository
     * @return JsonResponse
     */
    public function remove($id, SocioRepository $socioRepository): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        $socio = $socioRepository->findOneBy(['id' => $id]);
        $em->remove($socio);
        $em->flush();

        return new JsonResponse($socio);
    }
}
