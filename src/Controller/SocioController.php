<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SocioController extends AbstractController
{
    /**
     * @Route("/socios", name="socios")
     */
    public function index()
    {
        return $this->render('socio/list_socio.html.twig', [
            'controller_name' => 'SocioController',
        ]);
    }

    /**
     * @Route("/socios/{id}", name="socio_edit")
     * @param string $id
     * @return mixed
     */
    public function show($id)
    {
        $data = [
            'id' => $id
        ];

        return $this->render('socio/edit_socio.html.twig', [
            'data' => $data,
        ]);
    }
}
