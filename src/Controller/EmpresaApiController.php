<?php

namespace App\Controller;

use App\Entity\Empresa;
use App\Entity\Socio;
use App\Repository\EmpresaRepository;
use App\Repository\SocioRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EmpresaApiController extends AbstractController
{

    /**
     * @Route("/api/empresas", name="empresas_list", methods={"GET"})
     * @param EmpresaRepository $empresaRepository
     * @return JsonResponse
     */
    public function list(EmpresaRepository $empresaRepository): JsonResponse
    {
        return new JsonResponse($empresaRepository->findAll());
    }

    /**
     * @Route("/api/empresas/{id}", name="empresas_show", methods={"GET"})
     * @param string $id
     * @param EmpresaRepository $empresaRepository
     * @return JsonResponse
     */
    public function show($id, EmpresaRepository $empresaRepository): JsonResponse
    {
        return new JsonResponse($empresaRepository->findOneBy(['id' => $id]));
    }

    /**
     * @Route("/api/empresas", name="empresas_add", methods={"POST"})
     * @param Request $request
     * @param SocioRepository $socioRepository
     * @return JsonResponse
     */
    public function add(Request $request): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        $empresa = new Empresa();

        $empresa->setCnpj($request->request->get('cnpj'));
        $empresa->setNomeFantasia($request->request->get('nome_fantasia'));
        $empresa->setRazaoSocial($request->request->get('razao_social'));

        $em->persist($empresa);
        $em->flush();

        return new JsonResponse($empresa);
    }

    /**
     * @Route("/api/empresas/{id}", name="empresas_edit", methods={"PUT"})
     * @param string $id
     * @param Request $request
     * @param EmpresaRepository $empresaRepository
     * @return JsonResponse
     */
    public function edit($id, Request $request, EmpresaRepository $empresaRepository): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        $empresa = $empresaRepository->findOneBy(['id' => $id]);
        $empresa->setCnpj($request->request->get('cnpj'));
        $empresa->setNomeFantasia($request->request->get('nome_fantasia'));
        $empresa->setRazaoSocial($request->request->get('razao_social'));
        $em->flush();

        return new JsonResponse($empresa);
    }

    /**
     * @Route("/api/empresas/{id}", name="empresas_remove", methods={"DELETE"})
     * @param string $id
     * @param EmpresaRepository $empresaRepository
     * @return JsonResponse
     */
    public function remove($id, EmpresaRepository $empresaRepository): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        $empresa = $empresaRepository->findOneBy(['id' => $id]);
        $em->remove($empresa);
        $em->flush();

        return new JsonResponse($empresa);
    }
}
