# quadro-societario-vox
Foram feitos dois CRUD's simples: um de Empresa e outro de Sócio, associando o sócio à empresa. 
Os recursos são consumidos de uma API REST.

# Requisitos
 * Composer;
 * Yarn;
 * PostgreSQL;

# Setup 
 * Configure .env e doctrine.yaml para sua conexão local com o PostgreSQL; 
 ## Execute
 * composer install
 * yarn install
 * yarn encore dev
 * php bin/console doctrine:database:create
 * php bin/console doctrine:migrations:migrate
 * php bin/console server:start
