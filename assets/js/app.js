// CSS
require('../css/app.css');
require('bootstrap/dist/css/bootstrap.css');
require('@fortawesome/fontawesome-free/css/all.css');
require('bootstrap-select/dist/css/bootstrap-select.css');

// JS
var bootstrap = require('bootstrap/dist/js/bootstrap');
var popper = require('popper.js');
var fontawesome = require('@fortawesome/fontawesome-free/js/fontawesome');
var bootstrap_select = require('bootstrap-select/dist/js/bootstrap-select');

import $ from "jquery";
window.jQuery = $;
window.$ = $;